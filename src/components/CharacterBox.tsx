import { FunctionComponent } from 'react';

interface CharacterBoxProps {
  name: string,
  baseDpCost: number,
  eliteDpCost: {
    e0: number,
    e1: number,
    e2: number,
  },
  potentialDpCost: {
    p1: number,
    p2: number,
    p3: number,
    p4: number,
    p5: number,
    p6: number,
  },

}

const CharacterBox: FunctionComponent<CharacterBoxProps> = (props: CharacterBoxProps) => {
  const {

  } = props;

  return (
    <div></div>
  );
};

export default CharacterBox;