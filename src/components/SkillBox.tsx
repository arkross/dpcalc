import {
  FunctionComponent,
  useMemo,
  useEffect,
} from 'react';
import {
  Form,
} from 'react-bootstrap';
import skillJson from '../data/skills.json';

const allSkillData: Array<Skill> = skillJson;

interface SkillBoxProps {
  name: string,
  level: number,
  elite: number,
  onChange?: (selected: SelectedSkillLevel) => any,
}

const SkillBox: FunctionComponent<SkillBoxProps> = (props: SkillBoxProps) => {
  const {
    name,
    level,
    elite,
    onChange,
  } = props;

  const skillData: (Skill | boolean) = useMemo(() => {
    const rawData: (Skill | undefined) = allSkillData.find(el => el.name === name);
    if ( ! rawData) {
      return false;
    }
    const levels: Array<Level> = [
      {
        level: 1,
        spCost: rawData.spCost,
        initialSp: rawData.initialSp,
        duration: rawData.duration,
        dpGain: rawData.dpGain,
        dpGainPeriod: rawData.dpGainPeriod,
      }
    ];
    for (let levelIndex = 0; levelIndex < rawData.levels.length; levelIndex++) {
      const currentLevel = rawData.levels[levelIndex];
      const prevLevel = levels[levelIndex];
      levels.push({
        level: levelIndex + 2,
        spCost: (prevLevel.spCost || 0) + (currentLevel.spCost || 0),
        initialSp: (prevLevel.initialSp || 0) + (currentLevel.initialSp || 0),
        duration: (prevLevel.duration || 0) + (currentLevel.duration || 0),
        dpGain: (prevLevel.dpGain || 0) + (currentLevel.dpGain || 0),
        dpGainPeriod: (prevLevel.dpGainPeriod || 0) + (currentLevel.dpGainPeriod || 0),
      });
    }
    return {
      ...rawData,
      levels,
    };
  }, [name]);

  const getSelectedSkillLevel = (lv: number, el: number) => Math.min(lv, [4, 7, 10][el]);

  const getSelectedSkillData = (lv: number) => (skillData ? {
    ...skillData,
    ...skillData.levels[lv],
    level: lv,
  } : {
    name: 'Invalid Skill Data',
    activation: '',
    charge: '',
    level: 0,
    spCost: 0,
    duration: 0,
    initialSp: 0,
    dpGain: 0,
    dpGainPeriod: 0,
  });

  const handleChange = ({ target: { value }} : { target: { value: string }}) => {
    if (onChange && typeof onChange === 'function' && skillData) {
      onChange(getSelectedSkillData(parseInt(value)));
    }
  };

  useEffect(() => {
    if (onChange && typeof onChange === 'function' && skillData) {
      onChange(getSelectedSkillData(getSelectedSkillLevel(level, elite)));
    }
  }, [elite]);

  if (!skillData) {
    return (
      <div>Wrong skill name</div>
    );
  }

  const options: Array<SkillOption> = [];
  skillData.levels.forEach((currentLevel) => {
    if (currentLevel.level === undefined) {
      return null;
    }
    options.push({
      value: currentLevel.level,
      label: (currentLevel.level > 7) ? `M${currentLevel.level - 7}` : currentLevel.level,
      disabled: (
        (elite < 1 && currentLevel.level > 4)
        || (elite < 2 && currentLevel.level > 7)
      ),
    });
  });

  return (
    <Form>
      <Form.Group controlId="level">
        <Form.Label>Level</Form.Label>
        <Form.Control
          as="select"
          name="level"
          onChange={handleChange}
          value={getSelectedSkillLevel(level, elite)}
        >
          {options.map(option => (
            <option value={option.value} key={option.value} disabled={option.disabled}>{option.label}</option>
          ))}
        </Form.Control>
      </Form.Group>
    </Form>
  );
}

export default SkillBox;
