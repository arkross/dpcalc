declare interface Level {
  level?: number,
  spCost?: number,
  initialSp?: number,
  duration?: number,
  dpGain?: number,
  dpGainPeriod?: number
}

declare type ActivationType = string;

declare interface Skill {
  name: string,
  activation: string,
  charge: string,
  spCost: number,
  initialSp: number,
  duration: number,
  dpGain: number,
  dpGainPeriod: number,
  levels: Level[]
}

declare interface SelectedSkillLevel {
  name: string,
  activation: string,
  charge: string,
  spCost: number,
  initialSp: number,
  duration: number,
  dpGain: number,
  dpGainPeriod: number,
  level: number,
}

declare interface SkillOption {
  value: any,
  label: string | number,
  disabled?: boolean
}